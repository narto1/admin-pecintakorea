<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_setting extends Panel_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('layout');
		$this->load->model('Mod_front_setting');
	}

	public function banner() {
		$breadcrumb = array('Home'=>base_url(), 'active_pages'=>'Front Setting');
		$this->form_validation->set_rules('post_url_banner_redirect', 'URL Redirect banner', 'required');
		if($this->form_validation->run() == FALSE)
		{
			$view = array(
				'page'				=> 'pages/front_setting_banner',
				'title'				=> 'Dashboard',
				'all_post'			=> $this->Mod_front_setting->get_post(),
				'breadcrumb'		=> $breadcrumb,
				'banner'			=> $this->Mod_front_setting->get_banners()
			);
		}else{
			return $this->Mod_front_setting->edit_banner(array('id'=>$this->input->post('post_id_banner')));
		}

		$this->layout->view($view);
	}

	public function redirect_home()
	{
		return redirect();
	}
}