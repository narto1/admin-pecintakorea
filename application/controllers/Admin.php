<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Panel_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('layout');
		$this->load->model('Mod_admin');
	}

	public function index() {
		$breadcrumb = array('Home'=>base_url(),'Admin'=>base_url('admin'),'active_pages'=>'Setting');
        $view = array(
			'page'				=> 'pages/admin_setting',
			'title'				=> 'Admin Setting',
			'breadcrumb'		=> $breadcrumb
		);
		$this->layout->view($view);
	}

	public function change_password()
	{
		$this->form_validation->set_rules('post_current_password', 'Current Password', 'required');
		$this->form_validation->set_rules('post_new_password', 'New Password', 'required|min_length[8]');
		$this->form_validation->set_rules('post_conf_new_password', 'Conf New Password', 'required|min_length[8]');
		if($this->form_validation->run() == FALSE)
		{
			$this->index();
		}else{
			if ($this->input->post('post_new_password') != $this->input->post('post_conf_new_password')) {
				$this->session->set_flashdata('error', 'Password Confirmation doesnt match');
				return redirect('admin/change_password');
			}
			$field = array(
				'password' => $this->input->post('post_new_password')
			);
			return $this->Mod_admin->change_password($field);
		}
	}

	public function user_request()
	{
		$breadcrumb = array('Home'=>base_url(),'active_pages'=>'Request Film');
        $view = array(
			'page'				=> 'pages/user_request',
			'title'				=> 'Request Film',
			'breadcrumb'		=> $breadcrumb,
			'user_request'		=> $this->Mod_admin->get_request_message()
		);
		$this->layout->view($view);
	}

	public function delete_message($id)
	{
		return $this->Mod_admin->delete_message($id);
	}
}