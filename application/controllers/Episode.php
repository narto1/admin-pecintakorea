<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Episode extends Panel_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('layout');
		$this->load->model('Mod_episode');
	}

	public function index() {
		$breadcrumb = array('Home'=>base_url(),'Episode'=>base_url('episode'),'active_pages'=>'Overview');
		$view = array(
			'page'				=> 'pages/episode',
			'title'				=> 'List Series',
			'all_series'		=> $this->Mod_episode->get_all_series(),
			'breadcrumb'		=> $breadcrumb
		);

		$this->layout->view($view);
	}

	public function query()
	{
		return $this->Mod_episode->query();
	}

	public function view($id = null) {
		if($id === null)
		{
			return redirect('home');
		}
		
		$breadcrumb = array('Home'=>base_url(),'Episode'=>base_url('episode'),'active_pages'=>'View '.$this->Mod_episode->get_episode_row(array('id'=>$id))->judul.' Episode '.$this->Mod_episode->get_episode_row(array('id'=>$id))->episode);
		$this->Mod_episode->check_data_exist($id);
		$imdbid = $this->Mod_episode->get_episode_row(array('id'=>$id))->movie_imdb_id;
		$view = array(
			'page'				=> 'pages/view_episode',
			'title'				=> 'View Episode',
			'content'			=> $this->Mod_episode->get_episode_row(array('id'=>$id)),
			'all_episodes'		=> $this->Mod_episode->get_all_episodes(array('movie_imdb_id'=>$imdbid)),
			'breadcrumb'		=> $breadcrumb
		);
		$this->layout->view($view);
	}

	public function add($imdbid = null)
	{
		if($imdbid === null)
		{
			return redirect('home');
		}

		$breadcrumb = array('Home'=>base_url(),'Episode'=>base_url('episode'),'active_pages'=>'Add Episode '.$this->Mod_episode->get_series_row(array('movie_imdb_id'=>$imdbid))->title);
		$this->Mod_episode->required_field();
		if($this->form_validation->run() == FALSE)
		{
			$view = array(
				'page'				=> 'pages/addepisode',
				'title'				=> 'Add Episode',
				'imdbid'			=> $imdbid,
				'post_data'			=> $this->Mod_episode->get_series_row(array('movie_imdb_id'=>$imdbid)),
				'episode_data'		=> $this->Mod_episode->get_episode_row(array('movie_imdb_id'=>$imdbid)),
				'last_episode'		=> $this->Mod_episode->get_last_episode(array('movie_imdb_id'=>$imdbid)),
				'breadcrumb'		=> $breadcrumb
			);

			$this->layout->view($view);
		}else{
			$field = array(
				'movie_imdb_id' => $this->input->post('post_imdbid'),
				'judul' => $this->input->post('post_title'),
				'episode' => $this->input->post('post_episode'),
				'judul_episode' => $this->input->post('post_episode_title'),
				'url_download_1' => $this->input->post('post_url_download_1'),
				'url_download_2' => $this->input->post('post_url_download_2'),
				'url_download_3' => $this->input->post('post_url_download_3')
			);
			return $this->Mod_episode->add($field);
		}
	}

	public function edit($id = null)
	{
		if($id === null)
		{
			return redirect('home');
		}
		
		$breadcrumb = array('Home'=>base_url(),'Episode'=>base_url('episode'),'active_pages'=>'Edit '.$this->Mod_episode->get_episode_row(array('id'=>$id))->judul.' Episode '.$this->Mod_episode->get_episode_row(array('id'=>$id))->episode);
		$this->Mod_episode->check_data_exist($id);
		$imdbid = $this->Mod_episode->get_episode_row(array('id'=>$id))->movie_imdb_id;
		$this->Mod_episode->required_field();
		if($this->form_validation->run() == FALSE)
		{
			$view = array(
				'page'				=> 'pages/editepisode',
				'title'				=> 'Edit Episode',
				'id'				=> $id,
				'imdbid'			=> $imdbid,
				'post_data'			=> $this->Mod_episode->get_series_row(array('movie_imdb_id'=>$imdbid)),
				'episode_data'		=> $this->Mod_episode->get_episode_row(array('id'=>$id)),
				'breadcrumb'		=> $breadcrumb
			);

			$this->layout->view($view);
		}else{
			$field = array(
				'movie_imdb_id' => $this->input->post('post_imdbid'),
				'episode' => $this->input->post('post_episode'),
				'judul_episode' => $this->input->post('post_episode_title'),
				'url_download_1' => $this->input->post('post_url_download_1'),
				'url_download_2' => $this->input->post('post_url_download_2'),
				'url_download_3' => $this->input->post('post_url_download_3')
			);
			return $this->Mod_episode->edit($field,$id);
		}
	}

	public function delete($id = null)
	{
		if($imdbid === null)
		{
			return redirect('home');
		}
		
		return $this->Mod_episode->delete($id);
	}
}