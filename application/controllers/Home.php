<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Panel_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('layout');
		$this->load->model('Mod_home');
	}

	public function index() {
		$breadcrumb = array('Home'=>base_url(), 'active_pages'=>'Dashboard');
		$view = array(
			'page'				=> 'pages/home',
			'title'				=> 'Dashboard',
			'all_post'			=> $this->Mod_home->get_post(),
			'breadcrumb'		=> $breadcrumb
		);

		$this->layout->view($view);
	}

	public function redirect_home()
	{
		return redirect();
	}
}