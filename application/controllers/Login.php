<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Mod_login');
	}

	public function index() {
        // $password = 'anakanjing23';
        // $password = md5($password);
        // var_dump($password);exit();
		$this->check();
	}

	public function check()
	{
		if($this->session->userdata('is_logged'))
		{
			return redirect('home');
		}
		$this->form_validation->set_rules('post_username', 'Username', 'required');
		$this->form_validation->set_rules('post_password', 'Password', 'required');
		if($this->form_validation->run() == FALSE)
		{
			$data = array(
				'title'				=> 'Login'
			);
			$this->load->view('pages/login', $data);
		}else{
			$field = array(
				'username' => $this->input->post('post_username'),
				'password' => $this->input->post('post_password')
			);
			return $this->Mod_login->check_login($field);
		}
	}

	public function logout()
	{
		return $this->Mod_login->logout();
	}
}