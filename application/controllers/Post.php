<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends Panel_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('layout');
		$this->load->model('Mod_post');
	}

	public function index() {
		$this->add();
	}

	public function view($imdbid = null) {
		if($imdbid === null)
		{
			return redirect('home');
		}

		$breadcrumb = array('Home'=>base_url(),'Post'=>base_url('post'),'active_pages'=>'View '.$this->Mod_post->get_movie(array('movie_imdb_id'=>$imdbid))->title);
		$this->Mod_post->check_data_exist($imdbid);
		$type = $this->Mod_post->get_type(array('movie_imdb_id'=>$imdbid));
		if ($type === 'movie') {
			$view = array(
				'page'				=> 'pages/view_movie',
				'title'				=> 'View Movie',
				'imdbid'			=> $imdbid,
				'content'			=> $this->Mod_post->get_movie(array('movie_imdb_id'=>$imdbid)),
				'breadcrumb'		=> $breadcrumb
			);
			$this->layout->view($view);
		}else{
			$this->load->model('Mod_episode');
			$view = array(
				'page'				=> 'pages/view_series',
				'title'				=> 'View Series',
				'imdbid'			=> $imdbid,
				'content'			=> $this->Mod_post->get_movie(array('movie_imdb_id'=>$imdbid)),
				'all_episodes'		=> $this->Mod_episode->get_all_episodes(array('movie_imdb_id'=>$imdbid)),
				'breadcrumb'		=> $breadcrumb
			);
			$this->layout->view($view);
		}
	}

	public function add($imdbid = null)
	{
		if($imdbid === null && $this->input->post('post_imdbid') === null)
		{
			$imdbid = $this->input->post('imdbid');
		}elseif($this->input->post('post_imdbid')!=null){
			$imdbid = $this->input->post('post_imdbid');
		}
		//var_dump($imdbid);exit();
		$breadcrumb = array('Home'=>base_url(),'Post'=>base_url('post'),'active_pages'=>'Add '.$imdbid);

		$this->Mod_post->required_field($imdbid);
		if($this->form_validation->run() == FALSE)
		{
			$view = array(
				'page'				=> 'pages/post',
				'title'				=> 'Create Post',
				'imdbid'			=> $imdbid,
				'breadcrumb'		=> $breadcrumb
			);
			$content = $this->Mod_post->get_movie_imdb($imdbid);
			foreach ($content as $key => $value) {
				$view['content'.$key] = $value;
			}

			$this->layout->view($view);
		}else{
			$field = array(
				'movie_imdb_id' => $this->input->post('post_imdbid'),
				'title' => $this->input->post('post_title'),
				'jenis' => $this->input->post('post_type'),
				'url_trailer' => $this->input->post('post_url_trailer'),
				'url_image' => $this->input->post('post_poster'),
				'overview' => $this->input->post('post_plot'),
				'produksi' => $this->input->post('post_production'),
				'director' => $this->input->post('post_director'),
				'country' => $this->input->post('post_country'),
				'actor' => $this->input->post('post_actor'),
				'genre' => $this->input->post('post_genre'),
				'release_date' => $this->input->post('post_release_date'),
				'quality' => $this->input->post('post_quality'),
				'tag' => $this->input->post('post_tag'),
				'url_sub' => '#'
			);
			if($field['jenis'] === 'movie')
			{
				$field['url_download_1'] = $this->input->post('post_url_download_1');
				$field['url_download_2'] = $this->input->post('post_url_download_2');
				$field['url_download_3'] = $this->input->post('post_url_download_3');
			}
			return $this->Mod_post->add($field);
		}
	}

	public function edit($imdbid = null)
	{
		if($imdbid === null)
		{
			return redirect('home');
		}

		$breadcrumb = array('Home'=>base_url(),'Post'=>base_url('post'),'active_pages'=>'Edit '.$this->Mod_post->get_movie(array('movie_imdb_id'=>$imdbid))->title);
		$this->Mod_post->check_data_exist($imdbid);
		$this->Mod_post->required_field($imdbid);
		if($this->form_validation->run() == FALSE)
		{
			$view = array(
				'page'				=> 'pages/editpost',
				'title'				=> 'Edit Post',
				'imdbid'			=> $imdbid,
				'data_post'			=> $this->Mod_post->get_movie(array('movie_imdb_id'=>$imdbid)),
				'breadcrumb'		=> $breadcrumb
			);
			//var_dump($content);exit();

			$this->layout->view($view);
		}else{
			$field = array(
				'movie_imdb_id' => $this->input->post('post_imdbid'),
				'title' => $this->input->post('post_title'),
				'jenis' => $this->input->post('post_type'),
				'url_image' => $this->input->post('post_poster'),
				'overview' => $this->input->post('post_plot'),
				'produksi' => $this->input->post('post_production'),
				'director' => $this->input->post('post_director'),
				'country' => $this->input->post('post_country'),
				'actor' => $this->input->post('post_actor'),
				'genre' => $this->input->post('post_genre'),
				'release_date' => $this->input->post('post_release_date'),
				'quality' => $this->input->post('post_quality'),
				'tag' => $this->input->post('post_tag')
			);
			if($field['jenis'] === 'movie')
			{
				$field['url_download_1'] = $this->input->post('post_url_download_1');
				$field['url_download_2'] = $this->input->post('post_url_download_2');
				$field['url_download_3'] = $this->input->post('post_url_download_3');
			}
			return $this->Mod_post->edit($field, $field['movie_imdb_id']);
		}
	}

	public function set_status($status=null, $imdbid=null)
	{
		if($imdbid === null || $status === null)
		{
			return redirect('home');
		}

		return $this->Mod_post->set_status($status, $imdbid);
	}

}