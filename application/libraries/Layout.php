<?php
class Layout {
	private $CI;

	public function __construct() {
		$this->CI = &get_instance();
	}

	public function view($data = [], $layout = 'frame') {
		$this->CI->load->model('Mod_admin');
		$data['total_request'] = $this->CI->Mod_admin->get_count_request();
		$this->CI->load->view($layout, $data);
	}
}