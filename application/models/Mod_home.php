<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_home extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_post($where = array())
	{
		return $this->db->select('*')->order_by('id')->get_where('listmovie', $where)->result();
	}
}
?>