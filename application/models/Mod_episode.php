<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_episode extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_all_episodes($where = array())
	{
		return $this->db->select('*')->order_by('episode', 'asc')->get_where('episode', $where)->result();
	}

	public function get_episode_row($where = array())
	{
		return $this->db->select('*')->order_by('episode', 'desc')->limit(1)->get_where('episode', $where)->row();
	}

	public function get_last_episode($where = array())
	{
		if ($this->db->select('*')->get_where('episode', $where)->num_rows() < 1) {
			return null;
		}

		$episode = [];
		foreach ($this->db->select('*')->get_where('episode', $where)->result() as $key) {
			$this_episode = 0;
			if(strpos($key->episode, '-'))
			{
				$this_episode = explode('-', $key->episode);
				$this_episode = $this_episode[1];
			}else{
				$this_episode = $key->episode;
			}
			array_push($episode, $this_episode);
		}
		rsort($episode);
		return $episode[0];
	}

	public function get_all_series()
	{
		return $this->db->select('*')->get_where('listmovie', array('jenis'=>'series'))->result();
	}

	public function get_series_row($where = null)
	{
		$where['jenis'] = 'series';
		return $this->db->select('*')->get_where('listmovie', $where)->row();
	}

	public function add($field)
	{
		$slug = $this->get_series_row(array('movie_imdb_id'=>$field['movie_imdb_id']))->slug;
		$field['created'] = time();
		$field['created_by'] = $this->session->userdata('admin_username');
		$this->generate_link($field['url_download_1'],$field['url_download_2'],$field['url_download_3']);
		$field['url_sub'] = $this->upload_sub($slug, $field['episode']);
		if($this->check_rows($field['movie_imdb_id'], $field['episode']) === 0)
		{
			if ($this->db->insert('episode', $field)) {
				$this->set_last_episode($field['movie_imdb_id']);
				$this->session->set_flashdata('success', 'Posting Episode success');
				return redirect('episode');
			}else{
				$this->session->set_flashdata('error', 'Posting error');
				return redirect('episode');
			}
		}else{
			$this->session->set_flashdata('error', 'Post failed! This episode already exist');
			return redirect('episode');
		}
	}

	public function edit($field, $id)
	{
		$slug = $this->get_series_row(array('movie_imdb_id'=>$field['movie_imdb_id']))->slug;
		$field['updated'] = time();
		$field['updated_by'] = $this->session->userdata('admin_username');
		$this->generate_link($field['url_download_1'],$field['url_download_2'],$field['url_download_3']);
		if($_FILES["input_sub"]["name"]!=null)
		{
			$field['url_sub'] = $this->upload_sub($slug, $field['episode']);
		}
		if ($this->db->where(array('id'=>$id))->update('episode', $field)) {
			$this->set_last_episode($field['movie_imdb_id']);
			$this->session->set_flashdata('success', 'Episode updated');
			return redirect('episode');
		}else{
			$this->session->set_flashdata('error', 'Update error');
			return redirect('episode');
		}
	}

	public function delete($id)
	{
		if ($this->db->where(array('id'=>$id))->delete('episode')) {
			$this->session->set_flashdata('success', 'Delete Episode success');
			return redirect('episode');
		}else{
			$this->session->set_flashdata('error', 'Failed to Delete Episode, unknown error');
			return redirect('episode');
		}
	}

	public function check_rows($imdbid, $episode)
	{
		return $this->db->select('*')->get_where('episode',array('movie_imdb_id'=>$imdbid, 'episode'=>$episode))->num_rows();
	}

	public function upload_sub($newSlug, $episode)
	{
		if($_FILES["input_sub"]["name"] != null)
		{
			$dirname = UPLOAD_SUB_PATH.$newSlug."/";
			//var_dump();exit();
			if (!is_dir($dirname)) {
				if (mkdir($dirname, 0777, true)) {
					$config['upload_path']          = $dirname;
	                $config['allowed_types']        = 'vtt|srt';
	                $config['file_name']        	= $newSlug.$episode.'.'.pathinfo($_FILES["input_sub"]["name"])['extension'];

					$this->load->library('upload', $config);

					$this->upload->initialize($config);

	                if ( ! $this->upload->do_upload('input_sub'))
	                {
                        $this->session->set_flashdata('error',$this->upload->display_errors());
                        return redirect('post');
	                }
	                else
	                {
						return "assets/sub/".$newSlug."/".$config['file_name'];
	                }
				}else{
					redirect("prv/ps/gagalmkdir");
				}
			}else{
				$config['upload_path']          = $dirname;
	            $config['allowed_types']        = 'vtt|srt';
	            $config['file_name']        	= $newSlug.$episode.'.'.pathinfo($_FILES["input_sub"]["name"])['extension'];

				$this->load->library('upload', $config);

				$this->upload->initialize($config);

	            if ( ! $this->upload->do_upload('input_sub'))
	            {
	                $this->session->set_flashdata('error',$this->upload->display_errors());
	                return redirect('post');
	           	}else{
					return "assets/sub/".$newSlug."/".$config['file_name'];
	           	}
			}
			
		}else{
			return "#";
		}
	}

	public function query()
	{
		foreach ($this->db->select('*')->get_where('listmovie', array())->result() as $key) {
			if(strpos($key->release_date, '/'))
			{
				$newDate= str_replace('/', '-', $key->release_date);
				$this->db->set('release_date',$newDate);
				$this->db->where(array('movie_imdb_id'=>$key->movie_imdb_id))->update('listmovie');
			}
		}
	}

	public function check_data_exist($id)
	{
		if ($this->db->select('*')->get_where('episode',array('id'=>$id))->num_rows() === 0) {
			return redirect('');
		}
	}

	public function set_last_episode($imdbid)
	{
		$this->db->set('episode',$this->get_last_episode(array('movie_imdb_id'=>$imdbid)));
		return $this->db->where(array('movie_imdb_id'=>$imdbid))->update('listmovie');
	}

	function generate_link($urldownload1,$urldownload2,$urldownload3)
	{
		$hasGenerate = 0;
        $search_row=$this->db->select("*")->from("redirect")->where("url",$urldownload1)->get()->num_rows();
        if($search_row === 0)
        {
        	$md = md5($urldownload1);
        	$data_array = array(
        		"url" => $urldownload1,
        		"link_md5" =>$md,
        		"link_md5_md5" =>md5($md),
        		"note" => "url_download"
        	);
        	$this->db->insert("redirect",$data_array);
        }
        $search_row=$this->db->select("*")->from("redirect")->where("url",$urldownload2)->get()->num_rows();
        if($search_row === 0)
        {
        	$md = md5($urldownload2);
        	$data_array = array(
        		"url" => $urldownload2,
        		"link_md5" =>$md,
        		"link_md5_md5" =>md5($md),
        		"note" => "url_download"
        	);
        	$this->db->insert("redirect",$data_array);
        }
        $search_row=$this->db->select("*")->from("redirect")->where("url",$urldownload3)->get()->num_rows();
        if($search_row === 0)
        {
        	$md = md5($urldownload3);
        	$data_array = array(
        		"url" => $urldownload3,
        		"link_md5" =>$md,
        		"link_md5_md5" =>md5($md),
        		"note" => "url_download"
        	);
        	$this->db->insert("redirect",$data_array);
        }
	}

	public function required_field()
	{
		$this->form_validation->set_rules('post_episode', 'Episode', 'required');
		$this->form_validation->set_rules('post_episode_title', 'Episode Title', 'required');
		$this->form_validation->set_rules('post_url_download_1', 'URL Download 1', 'required');
		$this->form_validation->set_rules('post_url_download_2', 'URL Download 2', 'required');
		$this->form_validation->set_rules('post_url_download_3', 'URL Download 3', 'required');
		$this->form_validation->set_rules('post_imdbid', 'IMDB ID', 'required');
		$this->form_validation->set_rules('post_title', 'Title', 'required');
	}
}
?>