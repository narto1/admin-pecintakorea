<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_admin extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function change_password($field)
	{
		if ($this->check_admin_exist($this->session->userdata('admin_username'), $this->input->post('post_current_password')) === 1) {
			$this->db->set('password', md5($field['password']));
			if ($this->db->where('username',$this->session->userdata('admin_username'))->update('user')) {
				$this->session->set_flashdata('success', 'Password updated');
				return redirect('admin');
			}
		}else{
			$this->session->set_flashdata('error', 'Wrong Password');
			return redirect('admin/change_password');
		}
	}

	public function check_admin_exist($username, $password)
	{
		return $this->db->select('*')->get_where('user', array('username'=>$username,'password'=>md5($password)))->num_rows();
	}

	public function get_request_message()
	{
		return $this->db->select('*')->order_by('status', 'asc')->get_where('request', array())->result();
	}

	public function get_count_request()
	{
		return $this->db->select('*')->get_where('request', array('status'=>0))->num_rows();
	}

	public function read_all_messages()
	{
		foreach ($this->db->select('*')->get_where('request', array())->result() as $key) {
			if($key->status === '0')
			{
				$this->db->set('status', '1');
				$this->db->where('id', $key->id)->update('request');
			}
		}
	}

	public function delete_message($id)
	{
		if ($this->db->where('id', $id)->delete('request')) {
			$this->session->set_flashdata('success', 'Succesfully deleting message');
			return redirect('admin/user_request');
		}else{
			$this->session->set_flashdata('error', 'Failed, Something error');
			return redirect('admin/user_request');
		}
	}
}
?>