<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_post extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_movie($where = array())
	{
		return $this->db->select('*')->order_by('id')->get_where('listmovie', $where)->row();
	}

	public function get_type($where = array())
	{
		return $this->db->select('*')->get_where('listmovie', $where)->row()->jenis;
	}

	public function add($field)
	{
		$field['slug'] = $this->create_slug($field['title']);
		$field['created'] = time();
		$field['created_by'] = $this->session->userdata('admin_username');
		$field['url_image'] = $this->create_image($field['url_image'], $field['slug']);
		if($field['jenis'] === "movie")
		{
			$this->generate_link("download/".$field['slug'],$field['url_download_1'],$field['url_download_2'],$field['url_download_3']);
			$field['url_sub'] = $this->upload_sub($field['slug']);
		}
		if($this->check_rows($this->input->post('post_imdbid')) === 0)
		{
			if ($this->db->insert('listmovie', $field)) {
				if($field['jenis']==='movie')
				{
					$this->session->set_flashdata('success', 'Posting success');
					return redirect('post');
				}else{
					$this->session->set_flashdata('success', 'Posting success, please post first episode');
					return redirect('episode/add/'.$field['movie_imdb_id']);
				}
			}else{
				$this->session->set_flashdata('error', 'Posting error');
				return redirect('post');
			}
		}else{
			$this->session->set_flashdata('error', 'Post failed! This post already exist');
			return redirect('post');
		}
	}

	public function edit($field, $imdbid)
	{
		$field['slug'] = $this->create_slug($field['title']);
		$field['updated'] = time();
		$field['updated_by'] = $this->session->userdata('admin_username');
		$field['url_image'] = $this->create_image($field['url_image'], $field['slug']);
		if($field['jenis'] === "movie")
		{
			$this->generate_link("download/".$field['slug'],$field['url_download_1'],$field['url_download_2'],$field['url_download_3']);
			if($_FILES["input_sub"]["name"]!=null)
			{
				$field['url_sub'] = $this->upload_sub($field['slug']);
			}
		}
		if ($this->db->where(array('movie_imdb_id'=>$imdbid))->update('listmovie', $field)) {
			$this->session->set_flashdata('success', 'Post updated');
			return redirect('post');
		}else{
			$this->session->set_flashdata('error', 'Failed to updating post, unknown error');
			return redirect('post');
		}
	}

	public function check_rows($imdbid)
	{
		return $this->db->select('*')->get_where('listmovie',array('movie_imdb_id'=>$imdbid))->num_rows();
	}

	public function check_data_exist($imdbid)
	{
		if ($this->db->select('*')->get_where('listmovie',array('movie_imdb_id'=>$imdbid))->num_rows() === 0) {
			return redirect('');
		}
	}

	public function create_slug($title)
	{
		$slug = preg_replace("![^a-z0-9]+!i", "-", $title);
		$i=1;
		while($this->db->select("*")->from("listmovie")->where("slug",$slug)->get()->num_rows() === 1){
			$slug = $slug . "-" . $i++;
		}
		return $slug;
	}

	public function create_image($url_image,$slug){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type'=>'image/jpeg'));
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		curl_setopt($ch, CURLOPT_URL, $url_image);
		$src_image = imagecreatefromstring(curl_exec($ch));
		curl_close($ch);
		$new_image = imagecreatetruecolor(300, 400);
		imagecopyresampled($new_image, $src_image, 0, 0, 0, 0, 300, 400, imagesx($src_image), imagesy($src_image));
		imagedestroy($src_image);
		imagejpeg($new_image, $_SERVER['DOCUMENT_ROOT']."/assets/images/poster/".$slug.'.jpg');
		return "assets/images/poster/".$slug.'.jpg';
	}

	public function upload_sub($newSlug)
	{
		if($_FILES["input_sub"]["name"] != null)
		{
			$dirname = UPLOAD_SUB_PATH.$newSlug."/";
			//var_dump();exit();
			if (!is_dir($dirname)) {
				if (mkdir($dirname, 0777, true)) {
					$config['upload_path']          = $dirname;
	                $config['allowed_types']        = 'vtt|srt';
	                $config['file_name']        	= $newSlug.'.'.pathinfo($_FILES["input_sub"]["name"])['extension'];

					$this->load->library('upload', $config);

					$this->upload->initialize($config);

	                if ( ! $this->upload->do_upload('input_sub'))
	                {
                        $this->session->set_flashdata('error',$this->upload->display_errors());
                        return redirect('post');
	                }
	                else
	                {
						return "assets/sub/".$newSlug."/".$config['file_name'];
	                }
				}else{
					redirect("prv/ps/gagalmkdir");
				}
			}else{
				$config['upload_path']          = $dirname;
	            $config['allowed_types']        = 'vtt|srt';
	            $config['file_name']        	= $newSlug.'.'.pathinfo($_FILES["input_sub"]["name"])['extension'];

				$this->load->library('upload', $config);

				$this->upload->initialize($config);

	            if ( ! $this->upload->do_upload('input_sub'))
	            {
	                $this->session->set_flashdata('error',$this->upload->display_errors());
	                return redirect('post');
	           	}else{
					return "assets/sub/".$newSlug."/".$config['file_name'];
	           	}
			}
			
		}else{
			return "#";
		}
	}

	function generate_link($urlpage,$urldownload1,$urldownload2,$urldownload3)
	{
		$hasGenerate = 0;
        $search_row=$this->db->select("*")->from("redirect")->where("url",$urldownload1)->get()->num_rows();
        if($search_row === 0)
        {
        	$md = md5($urldownload1);
        	$data_array = array(
        		"url" => $urldownload1,
        		"link_md5" =>$md,
        		"link_md5_md5" =>md5($md),
        		"note" => "url_download"
        	);
        	$this->db->insert("redirect",$data_array);
        }
        $search_row=$this->db->select("*")->from("redirect")->where("url",$urldownload2)->get()->num_rows();
        if($search_row === 0)
        {
        	$md = md5($urldownload2);
        	$data_array = array(
        		"url" => $urldownload2,
        		"link_md5" =>$md,
        		"link_md5_md5" =>md5($md),
        		"note" => "url_download"
        	);
        	$this->db->insert("redirect",$data_array);
        }
        $search_row=$this->db->select("*")->from("redirect")->where("url",$urldownload3)->get()->num_rows();
        if($search_row === 0)
        {
        	$md = md5($urldownload3);
        	$data_array = array(
        		"url" => $urldownload3,
        		"link_md5" =>$md,
        		"link_md5_md5" =>md5($md),
        		"note" => "url_download"
        	);
        	$this->db->insert("redirect",$data_array);
        }
        $search_row=$this->db->select("*")->from("redirect")->where("url",$urlpage)->get()->num_rows();
        if($search_row === 0)
        {
        	$md = md5($urlpage);
        	$data_array = array(
        		"url" => $urlpage,
        		"link_md5" =>$md,
        		"link_md5_md5" =>md5($md),
        		"note" => "url_page"
        	);
        	$this->db->insert("redirect",$data_array);
        }
	}

	public function set_status($status, $imdbid)
	{
		$this->db->set('status', $status);
		$this->db->set('updated_by', $this->session->userdata('admin_username'));
		$this->db->set('updated', time());
		if($this->db->where(array('movie_imdb_id'=>$imdbid))->update('listmovie'))
		{
			$this->session->set_flashdata('success','Success, status updated to '.$status);
			return redirect('home');
		}else{
			$this->session->set_flashdata('error','Failed, unknown error');
			return redirect('home');
		}
	}

	public function get_movie_imdb($id)
	{
		$api_key = "cf865a5d";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'http://www.omdbapi.com/?i='.$id."&apikey=".$api_key."&plot=full");
		$content = curl_exec($ch);
		curl_close($ch);
		return json_decode($content);
	}

	public function required_field($imdbid = null)
	{
		if ($imdbid != null) {
			if ($this->get_movie_imdb($imdbid)->Response != 'False') {
				if($this->get_movie_imdb($imdbid)->Type === 'movie'){
					$this->form_validation->set_rules('post_url_download_1', 'URL Download 1', 'required');
					$this->form_validation->set_rules('post_url_download_2', 'URL Download 2', 'required');
					$this->form_validation->set_rules('post_url_download_3', 'URL Download 3', 'required');
				}
			}else{
				$this->form_validation->set_rules('post_url_download_1', 'URL Download 1', 'required');
				$this->form_validation->set_rules('post_url_download_2', 'URL Download 2', 'required');
				$this->form_validation->set_rules('post_url_download_3', 'URL Download 3', 'required');
			}
		}else{
			$this->form_validation->set_rules('post_url_download_1', 'URL Download 1', 'required');
			$this->form_validation->set_rules('post_url_download_2', 'URL Download 2', 'required');
			$this->form_validation->set_rules('post_url_download_3', 'URL Download 3', 'required');
		}
		$this->form_validation->set_rules('post_title', 'Title', 'required');
		$this->form_validation->set_rules('post_type', 'Type', 'required');
		$this->form_validation->set_rules('post_poster', 'URL Poster', 'required');
		$this->form_validation->set_rules('post_url_trailer', 'URL Trailer', 'required');
		$this->form_validation->set_rules('post_plot', 'Plot', 'required');
		$this->form_validation->set_rules('post_production', 'Production', 'required');
		$this->form_validation->set_rules('post_director', 'Director', 'required');
		$this->form_validation->set_rules('post_country', 'Country', 'required');
		$this->form_validation->set_rules('post_release_date', 'Release Date', 'required');
		$this->form_validation->set_rules('post_actor', 'Actor', 'required');
		$this->form_validation->set_rules('post_genre', 'Genre', 'required');
		$this->form_validation->set_rules('post_quality', 'Quality Video', 'required');
		$this->form_validation->set_rules('post_tag', 'Tag', 'required');
		$this->form_validation->set_rules('post_imdbid', 'Tag', 'required');
	}
}
?>