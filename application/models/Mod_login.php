<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_login extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function check_username($username)
	{
		if($this->db->select('*')->get_where('user', array('username'=>$username))->num_rows()===1)
		{
			return true;
		}else{
			return false;
		}
	}

	public function get_admin_data($field)
	{
		return $this->db->select('*')->get_where('user',array('username'=>$field['username'], 'password'=>md5($field['password'])))->row();
	}

	public function check_login($field)
	{
		if ($this->check_username($field['username'])) {
			if($this->db->select('*')->get_where('user', array('username'=>$field['username'], 'password'=>md5($field['password'])))->num_rows()===1)
			{
				$admin_data = $this->get_admin_data($field);
				$this->session->unset_userdata('post_username');
				$this->session->set_userdata(array(
					'admin_username'	=> $admin_data->username,
					'admin_name'		=> $admin_data->name,
					'is_logged'			=> true
				));
				return redirect('admin/home');
			}else{
				$this->session->set_flashdata('error', 'Wrong Password');
				$this->session->set_userdata(array('post_username'=>$field['username']));
				return redirect('login');
			}
		}else{
			$this->session->set_flashdata('error', 'Wrong Username');
			$this->session->set_userdata(array('post_username'=>$field['username']));
			return redirect('login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		return redirect('login');
	}
}
?>