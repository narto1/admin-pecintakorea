<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_front_setting extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_post($where = array())
	{
		return $this->db->select('*')->order_by('id')->get_where('listmovie', $where)->result();
	}

	public function get_banners($where = array())
	{
		foreach ($this->db->select('*')->order_by('id')->get_where('ads', $where)->result() as $key) {
			$data_banner[$key->kode] = $key->url_image;
			$data_banner[$key->kode.'_url'] = $key->url_ads;
			$data_banner[$key->kode.'_id'] = $key->id;
		};
		return $data_banner;
	}

	public function edit_banner($where)
	{
		if ($_FILES['post_image_banner']['name'] === "") {
			$this->db->set('url_ads', $this->input->post('post_url_banner_redirect'));
			if ($this->db->where($where)->update('ads')) {
				$this->session->set_flashdata('success', 'Edit Banner success');
				return redirect('front_setting/banner');
			}else{
				$this->session->set_flashdata('error', 'Edit Banner failed');
				return redirect('front_setting/banner');
			}
		}else{
			if ($this->do_upload()) {
				$this->db->set('url_ads', $this->input->post('post_url_banner_redirect'));
				if ($this->db->where($where)->update('ads')) {
					$this->session->set_flashdata('success', 'Edit Banner success');
					return redirect('front_setting/banner');
				}else{
					$this->session->set_flashdata('error', 'Edit Banner failed');
					return redirect('front_setting/banner');
				}
			}
		}
	}

    public function do_upload()
    {
        $config['upload_path']          = '../assets/images/banner/';
        // $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'].'assets/images/banner/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['overwrite']        	= true;

        $this->load->library('upload', $config);
		$this->upload->initialize($config);

        if ( ! $this->upload->do_upload('post_image_banner'))
        {
			$this->session->set_flashdata('error', $this->upload->display_errors());
			return redirect('front_setting/banner');
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            //print_r($data['upload_data']['file_name']);exit();
            $this->db->set('url_image', 'assets/images/banner/'.$data['upload_data']['file_name']);
			return true;
        }
}
}
?>