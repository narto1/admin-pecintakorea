
<?php if ($this->session->flashdata('error')): ?>
	<p class="alert alert-danger"><?php echo $this->session->flashdata('error') ?></p>
<?php endif ?>

<?php if ($this->session->flashdata('success')): ?>
	<p class="alert alert-success"><?php echo $this->session->flashdata('success') ?></p>
<?php endif ?>