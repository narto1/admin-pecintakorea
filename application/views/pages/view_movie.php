<div id="content-wrapper">

  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>
    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>

    <!-- DataTables Example -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        <?php echo $content->title .' Details' ?>
    	</div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-view" width="100%" cellspacing="0">
            <tr>
              <td>IMDB ID</td>
              <td><?php echo $content->movie_imdb_id ?></td>
            </tr>
            <tr>
              <td>Title</td>
              <td><?php echo $content->title ?></td>
            </tr>
            <tr>
              <td>Type</td>
              <td><?php echo $content->jenis ?></td>
            </tr>
            <tr>
              <td>URL Trailer</td>
              <td><?php echo $content->url_trailer ?></td>
            </tr>
            <tr>
              <td>URL Poster</td>
              <td><?php echo $content->url_image ?></td>
            </tr>
            <tr>
              <td>URL Download 1</td>
              <td><?php echo $content->url_download_1 ?></td>
            </tr>
            <tr>
              <td>URL Download 2</td>
              <td><?php echo $content->url_download_2 ?></td>
            </tr>
            <tr>
              <td>URL Download 3</td>
              <td><?php echo $content->url_download_3 ?></td>
            </tr>
            <tr>
              <td>Overview</td>
              <td><?php echo $content->overview ?></td>
            </tr>
            <tr>
              <td>Production</td>
              <td><?php echo $content->produksi ?></td>
            </tr>
            <tr>
              <td>Director</td>
              <td><?php echo $content->director ?></td>
            </tr>
            <tr>
              <td>Country</td>
              <td><?php echo $content->country ?></td>
            </tr>
            <tr>
              <td>Casts/Actors</td>
              <td><?php echo $content->actor ?></td>
            </tr>
            <tr>
              <td>Released Date</td>
              <td><?php echo $content->release_date ?></td>
            </tr>
            <tr>
              <td>Video Quality</td>
              <td><?php echo $content->quality ?></td>
            </tr>
            <tr>
              <td>Viewers</td>
              <td><?php echo $content->viewer ?></td>
            </tr>
            <tr>
              <td>Slug</td>
              <td><?php echo $content->slug ?></td>
            </tr>
            <tr>
              <td>Subtitle Download URL</td>
              <td><?php echo $content->url_sub ?></td>
            </tr>
            <tr>
              <td>Created</td>
              <td>Created By : <?php echo $content->created_by ?> At : <?php echo timestamp_to_date($content->created) ?></td>
            </tr>
            <tr>
              <td>Updated</td>
              <td>Updated By : <?php echo $content->updated_by ?> At : <?php echo timestamp_to_date($content->updated) ?></td>
            </tr>
            <tr>
              <td>Status</td>
              <td><?php echo $content->status ?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="card-footer text-right">
        <a href="<?php echo base_url('post/edit/'.$content->movie_imdb_id) ?>" class="btn btn-warning">Edit</a>
        <?php if ($content->status === 'publish'): ?>
          <a href="<?php echo base_url('post/set_status/unpublish/'.$content->movie_imdb_id) ?>" class="btn btn-secondary">Unpublish</a>
          <?php else: ?>
          <a href="<?php echo base_url('post/set_status/publish/'.$content->movie_imdb_id) ?>" class="btn btn-success">Publish</a>
        <?php endif ?>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->
</div>