<div id="content-wrapper">

  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>
    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>

    <!-- DataTables Example -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        List Series
    	</div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Title</th>
                <th>Episode</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($all_series as $key): ?>
                <tr>
          		<td><?php echo $key->title ?></td>
          		<td><?php echo $key->episode.' Episode ('.$key->progres.')' ?></td>
          		<td><small><?php echo $key->created_by.', At '.timestamp_to_date($key->created) ?></small></td>
          		<?php if (!empty($key->updated_by)): ?>
          			<td><small><?php echo $key->updated_by.', At '.timestamp_to_date($key->updated) ?></small></td>
          			<?php else: ?>
          			<td><small>No Updates!</td>
          		<?php endif ?>
          		<td>
          			<div>
                  <a href="<?php echo base_url('post/view/'.$key->movie_imdb_id) ?>" class="btn-sm btn-dark">View</a>
          			</div>
                <div class="mt-2">
                  <a href="<?php echo base_url('post/edit/'.$key->movie_imdb_id) ?>" class="btn-sm btn-warning">Edit</a>
                </div>
                <div class="mt-2">
                  <a href="<?php echo base_url('episode/add/'.$key->movie_imdb_id) ?>" class="btn-sm btn-primary">Add Eps</a>
                </div>
          			<div class='mt-2'>
          				<?php if ($key->status === 'publish'): ?>
          					<a href="<?php echo base_url('post/set_status/unpublish/'.$key->movie_imdb_id) ?>" class="btn-sm btn-secondary">Unpublish</a>
          					<?php else: ?>
          					<a href="<?php echo base_url('post/set_status/publish/'.$key->movie_imdb_id) ?>" class="btn-sm btn-success">Publish</a>
          				<?php endif ?>
          			</div>
          		</td>
                </tr>
        	<?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->
</div>