<div id="content-wrapper">
  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>

    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>
    <form class="card mb-3" action="<?php echo base_url('episode/add/'.$imdbid) ?>" method="post" id="submitform" enctype="multipart/form-data">
      <div class="card-header">
        <i class="fas fa-plus-square"></i>
        <?php 
        if (!isset($episode_data->episode)) {
          $last_episode = 'No Episode posted, please post first episode';
        }else{
          $last_episode = 'Last Episode : '.$last_episode;
        } 
        ?>
        Add Episode Form - <?php echo $post_data->title.' ('.$last_episode.')' ?>
      </div>
      <div class="card-body">
        <div class="form-group">
          <label>Episode</label>
          <input class="form-control" type="text" name="post_episode" value="<?php echo set_value('post_episode') ?>">
          <?php echo form_error('post_episode', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>Episode Title</label>
          <input class="form-control" type="text" name="post_episode_title" value="<?php echo set_value('post_episode_title') ?>">
          <?php echo form_error('post_episode_title', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>URL Download 1</label>
          <input class="form-control" type="text" name="post_url_download_1" value="<?php echo set_value('post_url_download_1') ?>">
          <?php echo form_error('post_url_download_1', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>URL Download 2</label>
          <input class="form-control" type="text" name="post_url_download_2" value="<?php echo set_value('post_url_download_2') ?>">
          <?php echo form_error('post_url_download_2', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>URL Download 3</label>
          <input class="form-control" type="text" name="post_url_download_3" value="<?php echo set_value('post_url_download_3') ?>">
          <?php echo form_error('post_url_download_3', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>Subtitle</label>
          <input type="file" class="form-control-file" name="input_sub">
          <small>Make sure subtitle format .vtt or srt</small>
          <?php echo form_error('post_tag', '<li class="text-danger">', '</li>'); ?>
        </div>
      </div>
      <div class="card-footer small text-muted text-right">
        <input type="hidden" name="post_imdbid" value="<?php echo $imdbid ?>">
        <input type="hidden" name="post_title" value="<?php echo $post_data->title ?>">
        <button type="submit" class="btn btn-primary mb-2">Submit</button>
      </div>
    </form>

  </div>
  <!-- /.container-fluid -->
</div>