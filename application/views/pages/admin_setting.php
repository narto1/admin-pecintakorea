<div id="content-wrapper">

  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>
    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>

    <!-- DataTables Example -->
    <form class="card mb-3" action="<?php echo base_url('admin/change_password') ?>" method="post">
      <div class="card-header">
        <i class="fas fa-plus-square"></i>
        Change Password
      </div>
      <div class="card-body">
        <div class="form-group">
          <label>Current Password</label>
          <input class="form-control" type="password" name="post_current_password">
          <?php echo form_error('post_current_password', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>New Password</label>
          <input class="form-control" type="password" name="post_new_password">
          <?php echo form_error('post_new_password', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>Confirm New Password</label>
          <input class="form-control" type="password" name="post_conf_new_password">
          <?php echo form_error('post_conf_new_password', '<li class="text-danger">', '</li>'); ?>
        </div>
      </div>
      <div class="card-footer small text-muted text-right">
        <button type="submit" class="btn btn-primary mb-2">Save</button>
      </div>
    </form>

  </div>
  <!-- /.container-fluid -->
</div>