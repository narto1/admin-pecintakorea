<div id="content-wrapper">
  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>

    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>
    <form class="card mb-3" action="<?php echo base_url('episode/edit/'.$id) ?>" method="post" id="submitform" enctype="multipart/form-data">
      <div class="card-header">
        <i class="fas fa-plus-square"></i>
        Edit Episode Form - <?php echo $post_data->title.' Episode '.$episode_data->episode ?>
      </div>
      <div class="card-body">
        <div class="form-group">
          <label>Episode</label>
          <?php if (set_value('post_episode')!=null): ?>
            <input class="form-control" type="text" name="post_episode" value="<?php echo set_value('post_episode') ?>">
            <?php else: ?>
            <input class="form-control" type="text" name="post_episode" value="<?php echo $episode_data->episode ?>">
          <?php endif ?>
          <?php echo form_error('post_episode', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>Episode Title</label>
          <?php if (set_value('post_episode_title')!=null): ?>
            <input class="form-control" type="text" name="post_episode_title" value="<?php echo set_value('post_episode_title') ?>">
            <?php else: ?>
            <input class="form-control" type="text" name="post_episode_title" value="<?php echo $episode_data->judul_episode ?>">
          <?php endif ?>
          <?php echo form_error('post_episode_title', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>URL Download 1</label>
          <?php if (set_value('post_url_download_1')!=null): ?>
            <input class="form-control" type="text" name="post_url_download_1" value="<?php echo set_value('post_url_download_1') ?>">
            <?php else: ?>
            <input class="form-control" type="text" name="post_url_download_1" value="<?php echo $episode_data->url_download_1 ?>">
          <?php endif ?>
          <?php echo form_error('post_url_download_1', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>URL Download 2</label>
          <?php if (set_value('post_url_download_2')!=null): ?>
            <input class="form-control" type="text" name="post_url_download_2" value="<?php echo set_value('post_url_download_2') ?>">
            <?php else: ?>
            <input class="form-control" type="text" name="post_url_download_2" value="<?php echo $episode_data->url_download_2 ?>">
          <?php endif ?>
          <?php echo form_error('post_url_download_2', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>URL Download 3</label>
          <?php if (set_value('post_url_download_3')!=null): ?>
            <input class="form-control" type="text" name="post_url_download_3" value="<?php echo set_value('post_url_download_3') ?>">
            <?php else: ?>
            <input class="form-control" type="text" name="post_url_download_3" value="<?php echo $episode_data->url_download_3 ?>">
          <?php endif ?>
          <?php echo form_error('post_url_download_3', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>Subtitle</label>
          <input type="file" class="form-control-file" name="input_sub">
          <?php if ($episode_data->url_sub != "#"): ?>
            <a href="<?php echo $episode_data->url_sub ?>"><small>Subtitle URL : <?php echo $episode_data->url_sub ?></small></a><br>
          <?php endif ?>
          <small>Make sure subtitle format .vtt or srt</small>
          <?php echo form_error('post_tag', '<li class="text-danger">', '</li>'); ?>
        </div>
      </div>
      <div class="card-footer small text-muted text-right">
        <input type="hidden" name="post_imdbid" value="<?php echo $imdbid ?>">
        <input type="hidden" name="post_title" value="<?php echo $post_data->title ?>">
        <button type="submit" class="btn btn-primary mb-2">Submit</button>
      </div>
    </form>

  </div>
  <!-- /.container-fluid -->
</div>