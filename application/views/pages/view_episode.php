<div id="content-wrapper">

  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>
    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>

    <!-- DataTables Example -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        <?php echo $content->judul .' Episode '.$content->episode.' Details' ?>
    	</div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-view" width="100%" cellspacing="0">
            <tr>
              <td>IMDB ID</td>
              <td><?php echo $content->movie_imdb_id ?></td>
            </tr>
            <tr>
              <td>Title</td>
              <td><?php echo $content->judul ?></td>
            </tr>
            <tr>
              <td>Episode</td>
              <td><?php echo $content->episode ?></td>
            </tr>
            <tr>
              <td>Episode Title</td>
              <td><?php echo $content->judul_episode ?></td>
            </tr>
            <tr>
              <td>URL Download 1</td>
              <td><?php echo $content->url_download_1 ?></td>
            </tr>
            <tr>
              <td>URL Download 2</td>
              <td><?php echo $content->url_download_2 ?></td>
            </tr>
            <tr>
              <td>URL Download 3</td>
              <td><?php echo $content->url_download_3 ?></td>
            </tr>
            <tr>
              <td>URL Download Subtitle</td>
              <td><?php echo $content->url_sub ?></td>
            </tr>
            <tr>
              <td>Created</td>
              <td>Created By : <?php echo $content->created_by ?> At : <?php echo timestamp_to_date($content->created) ?></td>
            </tr>
            <tr>
              <td>Updated</td>
              <td>Updated By : <?php echo $content->updated_by ?> At : <?php echo timestamp_to_date($content->updated) ?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="card-footer small text-right">
        <a href="<?php echo base_url('episode/edit/'.$content->id) ?>" class="btn btn-warning">Edit</a>
        <a href="<?php echo base_url('episode/delete/'.$content->id) ?>" class="btn btn-danger">Delete</a>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        <?php echo 'Other ' . $content->judul .' Episodes' ?>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Episode</th>
                <th>Created</th>
                <th>Updated</th>
                <th class="text-right">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($all_episodes as $key): ?>
                <?php if ($key->id != $content->id): ?>
                  <tr>
                    <td><?php echo $key->episode ?></td>
                    <td><small><?php echo $key->created_by.', At '.timestamp_to_date($key->created) ?></small></td>
                    <?php if (!empty($key->updated_by)): ?>
                      <td><small><?php echo $key->updated_by.', At '.timestamp_to_date($key->updated) ?></small></td>
                      <?php else: ?>
                      <td><small>No Updates!</td>
                    <?php endif ?>
                    <td>
                      <div class="text-right">
                        <a href="<?php echo base_url('episode/view/'.$key->id) ?>" class="btn-sm btn-primary">View</a>
                        <a href="<?php echo base_url('episode/edit/'.$key->id) ?>" class="btn-sm btn-warning">Edit</a>
                        <a href="<?php echo base_url('episode/delete/'.$key->id) ?>" class="btn-sm btn-danger">Delete</a>
                      </div>
                    </td>
                  </tr>
                <?php endif ?>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->
</div>