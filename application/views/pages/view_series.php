<div id="content-wrapper">

  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>
    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>

    <!-- DataTables Example -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        <?php echo $content->title .' Details' ?>
    	</div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-view" width="100%" cellspacing="0">
            <tr>
              <td>IMDB ID</td>
              <td><?php echo $content->movie_imdb_id ?></td>
            </tr>
            <tr>
              <td>Title</td>
              <td><?php echo $content->title ?></td>
            </tr>
            <tr>
              <td>Type</td>
              <td><?php echo $content->jenis ?></td>
            </tr>
            <tr>
              <td>URL Trailer</td>
              <td><?php echo $content->url_trailer ?></td>
            </tr>
            <tr>
              <td>URL Poster</td>
              <td><?php echo $content->url_image ?></td>
            </tr>
            <tr>
              <td>Episodes</td>
              <td><?php echo $content->episode.' ('.$content->progres.') ' ?></td>
            </tr>
            <tr>
              <td>Overview</td>
              <td><?php echo $content->overview ?></td>
            </tr>
            <tr>
              <td>Production</td>
              <td><?php echo $content->produksi ?></td>
            </tr>
            <tr>
              <td>Director</td>
              <td><?php echo $content->director ?></td>
            </tr>
            <tr>
              <td>Country</td>
              <td><?php echo $content->country ?></td>
            </tr>
            <tr>
              <td>Casts/Actors</td>
              <td><?php echo $content->actor ?></td>
            </tr>
            <tr>
              <td>Released Date</td>
              <td><?php echo $content->release_date ?></td>
            </tr>
            <tr>
              <td>Viewers</td>
              <td><?php echo $content->viewer ?></td>
            </tr>
            <tr>
              <td>Slug</td>
              <td><?php echo $content->slug ?></td>
            </tr>
            <tr>
              <td>Created</td>
              <td>Created By : <?php echo $content->created_by ?> At : <?php echo timestamp_to_date($content->created) ?></td>
            </tr>
            <tr>
              <td>Updated</td>
              <td>Updated By : <?php echo $content->updated_by ?> At : <?php echo timestamp_to_date($content->updated) ?></td>
            </tr>
            <tr>
              <td>Status</td>
              <td><?php echo $content->status ?></td>
            </tr>
          </table>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Episode</th>
                <th>Created</th>
                <th>Updated</th>
                <th class="text-right">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($all_episodes as $key): ?>
                <tr>
                  <td><?php echo $key->episode ?></td>
                  <td><small><?php echo $key->created_by.', At '.timestamp_to_date($key->created) ?></small></td>
                  <?php if (!empty($key->updated_by)): ?>
                    <td><small><?php echo $key->updated_by.', At '.timestamp_to_date($key->updated) ?></small></td>
                    <?php else: ?>
                    <td><small>No Updates!</td>
                  <?php endif ?>
                  <td>
                    <div class="text-right">
                      <a href="<?php echo base_url('episode/view/'.$key->id) ?>" class="btn-sm btn-primary">View</a>
                      <a href="<?php echo base_url('episode/edit/'.$key->id) ?>" class="btn-sm btn-warning">Edit</a>
                      <a href="<?php echo base_url('episode/delete/'.$key->id) ?>" class="btn-sm btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer text-right">
        <a href="<?php echo base_url('episode/add/'.$content->movie_imdb_id) ?>" class="btn btn-primary">Add Episode</a>
        <a href="<?php echo base_url('post/edit/'.$content->movie_imdb_id) ?>" class="btn btn-warning">Edit</a>
        <?php if ($content->progres === 'ongoing'): ?>
          <a href="<?php echo base_url('episode/set_status/complete/'.$content->movie_imdb_id) ?>" class="btn btn-success">Set Episode Complete</a>
          <?php else: ?>
          <a href="<?php echo base_url('episode/set_status/ongoing/'.$content->movie_imdb_id) ?>" class="btn btn-success">Set Episode On-Going</a>
        <?php endif ?>
        <?php if ($content->status === 'publish'): ?>
          <a href="<?php echo base_url('post/set_status/unpublish/'.$content->movie_imdb_id) ?>" class="btn btn-secondary">Unpublish</a>
          <?php else: ?>
          <a href="<?php echo base_url('post/set_status/publish/'.$content->movie_imdb_id) ?>" class="btn btn-success">Publish</a>
        <?php endif ?>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->
</div>