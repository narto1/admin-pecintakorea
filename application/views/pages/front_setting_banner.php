<div id="content-wrapper">

  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>
    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>

    <!-- DataTables Example -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        All Banner
    	</div>
      <div class="card-body">
          <div class="row">
            <div class="col-6">
              <img style="width: 100%" src="<?php echo base_url('../'.$banner['banner_top_1']) ?>">
              <button onclick="show_banner_form('<?php echo base_url('../'.$banner['banner_top_1']) ?>','<?php echo $banner['banner_top_1_url']?>', '<?php echo $banner['banner_top_1_id'] ?>')" class="btn btn-sm btn-primary w-100">Ganti</button>
            </div>
            <div class="col-6">
              <img style="width: 100%" src="<?php echo base_url('../'.$banner['banner_top_2']) ?>">
              <button onclick="show_banner_form('<?php echo base_url('../'.$banner['banner_top_2']) ?>','<?php echo $banner['banner_top_2_url']?>', '<?php echo $banner['banner_top_2_id'] ?>')" class="btn btn-sm btn-primary w-100">Ganti</button>
            </div>
          </div>
          <div class="row">
            <div class="col-9 mt-4">
              <img src="<?php echo base_url('assets/images/content-vector.jpeg') ?>" style="width: 100%;max-height: 400px;">
            </div>
            <div class="col-3 mt-4">
              <div class="row">
                <div class="col-12">
                  <img style="width: 100%" src="<?php echo base_url('../'.$banner['banner_right_1']) ?>">
                  <button onclick="show_banner_form('<?php echo base_url('../'.$banner['banner_right_1']) ?>','<?php echo $banner['banner_right_1_url']?>', '<?php echo $banner['banner_right_1_id'] ?>')" class="btn btn-sm btn-primary w-100">Ganti</button>
                </div>
                <div class="col-12">
                  <img class="mt-2" style="width: 100%" src="<?php echo base_url('../'.$banner['banner_right_2']) ?>">
                  <button onclick="show_banner_form('<?php echo base_url('../'.$banner['banner_right_2']) ?>','<?php echo $banner['banner_right_2_url']?>', '<?php echo $banner['banner_right_2_id'] ?>')" class="btn btn-sm btn-primary w-100">Ganti</button>
                </div>
                <div class="col-12">
                  <img class="mt-2" style="width: 100%" src="<?php echo base_url('../'.$banner['banner_right_3']) ?>">
                  <button onclick="show_banner_form('<?php echo base_url('../'.$banner['banner_right_3']) ?>','<?php echo $banner['banner_right_3_url']?>', '<?php echo $banner['banner_right_3_id'] ?>')" class="btn btn-sm btn-primary w-100">Ganti</button>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->
</div>
<div class="row form-banner-fixed d-none" id="banner-form">
  <div class="col-2"></div>
  <div class="col-8">
    <div class="">
      <form action="<?php echo base_url('front_setting/banner') ?>" enctype="multipart/form-data" method="post">
        <div class="form-group row wrap-banner">
          <div class="col-12">
            <img id="url-image" style="max-width: 100%;max-height: 200px;" src="<?php echo $banner['banner_top_1'] ?>">
          </div>
          <div class="col-12 mt-1">
            <input class="form-control" type="file" name="post_image_banner">
          </div>
          <div class="col-12 mt-1">
            <input class="form-control" type="text" name="post_url_banner_redirect" id="url-ads">
            <input type="hidden" name="post_id_banner" id="id-banner">
            <?php echo form_error('post_url_banner_redirect', '<li class="text-danger">', '</li>'); ?>
          </div>
          <div class="col-12 mt-3 text-right">
            <input class="btn btn-lg btn-success" type="submit" name="submit" placeholder="Redirect URL">
            <button onclick="hide_banner_form()" class="btn btn-lg btn-secondary" type="button">Cancel</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="col-2"></div>
</div>
<script type="text/javascript">
  function show_banner_form(urlimage, urlads, idbanner)
  {
    $('#url-image').attr('src', urlimage);
    $('#url-ads').val(urlads);
    $('#id-banner').val(idbanner);
    $('#banner-form').removeClass('d-none');
  }
  function hide_banner_form()
  {
    $('#banner-form').addClass('d-none');
  }
</script>