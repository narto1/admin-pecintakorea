<div id="content-wrapper">
  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>

    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>

    <form class="card mb-3" action="<?php echo base_url('post/add') ?>" method="post">
      <div class="card-header">
        <i class="fas fa-plus-square"></i>
        Generate IMDB ID
      </div>
      <div class="card-body">
        <div class="form-group">
          <label>IMDB ID</label>
          <input class="form-control" type="text" name="imdbid">
          <small id="emailHelp" class="form-text text-muted">Please insert IMDB ID movie. <i>EX : tt1234567</i></small>
          <?php if ($imdbid != null && $contentResponse === 'False'): ?>
            <li class="text-danger"><?php echo $contentError ?></li>
          <?php endif ?>
        </div>
      </div>
      <div class="card-footer small text-muted text-right">
        <button type="submit" class="btn btn-primary mb-2">Generate</button>
      </div>
    </form>

    <form class="card mb-3" action="<?php echo base_url('post/add') ?>" method="post" id="submitform" enctype="multipart/form-data">
      <div class="card-header">
        <i class="fas fa-plus-square"></i>
        Post Form
      </div>
      <div class="card-body">
        <div class="form-group">
          <label>Title</label>
          <?php if (isset($contentTitle)): ?>
            <input class="form-control" type="text" name="post_title" value="<?php echo $contentTitle ?>">
            <?php else: ?>
            <input class="form-control" type="text" name="post_title" value="<?php echo set_value('post_title') ?>">
          <?php endif ?>
          <?php echo form_error('post_title', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group">
          <label>Type</label>
          <?php if (isset($contentType)): ?>
            <input class="form-control" type="text" disabled value="<?php echo $contentType ?>">
            <input class="form-control" type="hidden" name="post_type" value="<?php echo $contentType ?>">
            <?php else: ?>
            <input class="form-control" type="text" name="post_type" value="<?php echo set_value('post_type') ?>">
          <?php endif ?>
          <?php echo form_error('post_type', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group row">
          <div class="col-md-8">
            <label>URL Poster</label>
            <?php if (isset($contentPoster)): ?>
              <input id="inp-img-poster" class="form-control" oninput="load_image('#poster-image')" type="text" name="post_poster" value="<?php echo $contentPoster ?>">
              <?php else: ?>
              <input id="inp-img-poster" class="form-control" oninput="load_image('#poster-image')" type="text" name="post_poster" value="<?php echo set_value('post_poster') ?>">
            <?php endif ?>
            <?php echo form_error('post_poster', '<li class="text-danger">', '</li>'); ?>
          </div>
          <div class="col-md-4">
            <?php if (isset($contentPoster)): ?>
              <img style="width: 200px" id="poster-image" src="<?php echo $contentPoster ?>">
              <?php else: ?>
              <img style="width: 200px" id="poster-image" src="">
            <?php endif ?>
          </div>
        </div>
        <div class="form-group">
          <label>URL Tralier</label>
          <input class="form-control" type="text" name="post_url_trailer" value="<?php echo set_value('post_url_trailer') ?>">
          <?php echo form_error('post_url_trailer', '<li class="text-danger">', '</li>'); ?>
        </div>
        <?php  
          if (isset($contentType)) {
            if ($contentType === 'series') {
              $url_download_form = 1;
            }
          }
          if (set_value('post_type') === 'series') {
            $url_download_form = 1;
          }
        ?>
        <?php if (!isset($url_download_form)): ?>
          <div class="form-group">
            <label>URL Download 1</label>
            <input class="form-control" type="text" name="post_url_download_1" value="<?php echo set_value('post_url_download_1') ?>">
            <?php echo form_error('post_url_download_1', '<li class="text-danger">', '</li>'); ?>
          </div>
          <div class="form-group">
            <label>URL Download 2</label>
            <input class="form-control" type="text" name="post_url_download_2" value="<?php echo set_value('post_url_download_2') ?>">
            <?php echo form_error('post_url_download_2', '<li class="text-danger">', '</li>'); ?>
          </div>
          <div class="form-group">
            <label>URL Download 3</label>
            <input class="form-control" type="text" name="post_url_download_3" value="<?php echo set_value('post_url_download_3') ?>">
            <?php echo form_error('post_url_download_3', '<li class="text-danger">', '</li>'); ?>
          </div>
        <?php endif ?>
        <div class="form-group">
          <label>Plot</label>
          <?php if (isset($contentPlot)): ?>
            <textarea oninput="CekFormSinopsis()" name="post_plot" class="form-control" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 137px;"><?php echo $contentPlot ?></textarea>
            <?php else: ?>
            <textarea oninput="CekFormSinopsis()" name="post_plot" class="form-control" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 137px;"> <?php echo set_value('post_plot') ?></textarea>
          <?php endif ?>
          <?php echo form_error('post_plot', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group row">
          <div class="col-md-6">
            <label>Production</label>
            <?php if (isset($contentProduction)): ?>
              <input class="form-control" type="text" name="post_production" value="<?php echo $contentProduction ?>">
              <?php else: ?>
              <input class="form-control" type="text" name="post_production" value="<?php echo set_value('post_production') ?>">
            <?php endif ?>
            <?php echo form_error('post_production', '<li class="text-danger">', '</li>'); ?>
          </div>
          <div class="col-md-6">
            <label>Director</label>
            <?php if (isset($contentDirector)): ?>
              <input class="form-control" type="text" name="post_director" value="<?php echo $contentDirector ?>">
              <?php else: ?>
              <input class="form-control" type="text" name="post_director" value="<?php echo set_value('post_director') ?>">
            <?php endif ?>
            <?php echo form_error('post_director', '<li class="text-danger">', '</li>'); ?>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-6">
            <label>Country</label>
            <?php if (isset($contentCountry)): ?>
              <input class="form-control" type="text" name="post_country" value="<?php echo $contentCountry ?>">
              <?php else: ?>
              <input class="form-control" type="text" name="post_country" value="<?php echo set_value('post_country') ?>">
            <?php endif ?>
            <?php echo form_error('post_country', '<li class="text-danger">', '</li>'); ?>
          </div>
          <div class="col-md-6">
            <label>Release Date</label>
            <?php if (isset($contentReleased)): ?>
              <input class="form-control" type="date" name="post_release_date" value="<?php echo str_replace('/','-',date('Y/m/d',strtotime($contentReleased))) ?>">
              <?php else: ?>
              <input class="form-control" type="text" name="post_release_date" value="<?php echo set_value('post_release_date') ?>">
            <?php endif ?>
            <?php echo form_error('post_release_date', '<li class="text-danger">', '</li>'); ?>
          </div>
        </div>
        <div class="form-group">
          <label>Cast/Actor</label>
          <?php if (isset($contentActors)): ?>
            <input class="form-control" type="text" name="post_actor" value="<?php echo $contentActors ?>">
            <?php else: ?>
            <input class="form-control" type="text" name="post_actor" value="<?php echo set_value('post_actor') ?>">
          <?php endif ?>
          <?php echo form_error('post_actor', '<li class="text-danger">', '</li>'); ?>
        </div>
        <div class="form-group row">
          <div class="col-md-6">
            <label>Genre</label>
            <?php if (isset($contentGenre)): ?>
              <input class="form-control" type="text" name="post_genre" value="<?php echo $contentGenre ?>">
              <?php else: ?>
              <input class="form-control" type="text" name="post_genre" value="<?php echo set_value('post_genre') ?>">
            <?php endif ?>
            <?php echo form_error('post_genre', '<li class="text-danger">', '</li>'); ?>
          </div>
          <div class="col-md-6">
            <label>Video Quality</label>
            <select name="post_quality" class="form-control">
              <option value="bluray">bluray</option>
              <option value="cam">cam</option>
              <option value="fhd">fhd</option>
              <option value="hd">hd</option>
              <option value="sd">sd</option>
            </select>
            <?php echo form_error('post_quality', '<li class="text-danger">', '</li>'); ?>
          </div>
        </div>
        <?php if (!isset($url_download_form)): ?>
            <div class="form-group">
            <label>Subtitle</label>
            <input type="file" class="form-control-file" name="input_sub">
            <small>Make sure subtitle format .vtt or srt</small>
            <?php echo form_error('post_tag', '<li class="text-danger">', '</li>'); ?>
          </div>
        <?php endif ?>
        <div class="form-group" id="parentFormTag">
          <label>Tag</label>
          <input type="text" class="form-control" id="tagsinput"/>
          <input type="hidden" id="inputTagsHiden" name="post_tag">
          <?php echo form_error('post_tag', '<li class="text-danger">', '</li>'); ?>
        </div>
      </div>
      <div class="card-footer small text-muted text-right">
        <?php if ($imdbid != null): ?>
          <input type="hidden" name="post_imdbid" value="<?php echo $imdbid ?>">
          <?php else: ?>
          <input type="hidden" name="post_imdbid" value="<?php echo set_value('post_imdbid') ?>">
        <?php endif ?>
        <button type="submit" class="btn btn-primary mb-2">Submit</button>
      </div>
    </form>
    <?php if (isset($contentTitle)): ?>
      <script type="text/javascript">TagDefault("<?php echo $contentTitle ?>")</script>
      <?php else: ?>
      <script type="text/javascript">TagDefault("<?php echo set_value('post_title') ?>")</script>
    <?php endif ?>

  </div>
  <!-- /.container-fluid -->
</div>
<script type="text/javascript">
  function load_image(img_id)
  {
    $(img_id).attr('src', $('#inp-img-poster').val());
  }
</script>