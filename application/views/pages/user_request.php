<div id="content-wrapper">

  <div class="container-fluid">
    <?php $this->load->view('static/alert') ?>
    <!-- Breadcrumbs-->
    <?php $this->load->view('static/breadcrumb') ?>

    <!-- DataTables Example -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        All Request
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>From</th>
                <th colspan="2">Message</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($user_request as $key): ?>
                <?php if ($key->status === '0'): ?>
                  <tr style="background-color: #ded7d7">
                  <?php else: ?>
                  <tr>
                <?php endif ?>
              <td><?php echo $key->sender ?></td>
              <td><?php echo $key->messages ?></td>
              <td>
                <div>
                  <a href="<?php echo base_url('admin/delete_message/'.$key->id) ?>" class="btn-sm btn-danger">delete</a>
                </div>
              </td>
                </tr>
          <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->
</div>
<?php $this->Mod_admin->read_all_messages() ?>