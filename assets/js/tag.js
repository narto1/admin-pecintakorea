var tags = [];
var judul = ['download-film-judulfilm','film-terbaru-judulfilm','drama-korea-judulfilm-subindo', 'sinopsis-judulfilm','nonton-movie-judulfilm','nonton-serial-judulfilm','nonton-film-gratis-judulfilm','free-download-film-judulfilm','judulfilm-sub-indo'];
	$(document).ready(function(){
		$( "#tagsinput" ).keydown(function( event ) {
		  var addTagSpan = $.trim($( "#tagsinput" ).val());
		  if ( event.which == 13 || event.which == 9 ) {
		  	if(addTagSpan == "" || addTagSpan == null || addTagSpan == " "){
		  		return false;
		  	}else{
				var i;
				if(tags.length > 0)
				{
					for (i = 0 ; i < tags.length; i++) {
						if(addTagSpan == tags[i])
						{
							alert("Tag "+addTagSpan+"sudah ada");
						}else{
		  					var tagSpanId = addTagSpan.replace(" ", "-");
						  	tags.push(tagSpanId);
							$("#inputTagsHiden").val(tags.join(","));
						  	var quotes = '"';
						  	$( "#tagsinput" ).val("");
						  	return $( "<div id='Tags"+tagSpanId+"'></div>" ).attr("style","background-color:#fff;color:black;font-size:14px; position:relative;display:inline-block;margin-right:5px;margin-top:3px")
				                   
				                   .append( "<p style='margin:0px 25px 0px 0px;padding:5px;'>"+addTagSpan+"</p><span onclick='onClickRemoveTags("+quotes+tagSpanId+quotes+")' style='position:absolute;right:3px;top:3px;cursor:pointer'><img width='20px' src='https://www.freeiconspng.com/uploads/silver-close-button-png-15.png'></span>")
				                   .appendTo($("#parentFormTag"));
						}
					}
				}else{
		  			var tagSpanId = addTagSpan.replace(" ", "-");
					tags.push(tagSpanId);
					$("#inputTagsHiden").val(tags.join(","));
					var quotes = '"';
					$( "#tagsinput" ).val("");
					return $( "<div id='Tags"+tagSpanId+"'></div>" ).attr("style","background-color:#fff;color:black;font-size:14px; position:relative;display:inline-block;margin-right:5px;margin-top:3px")
				     
				    .append( "<p style='margin:0px 25px 0px 0px;padding:5px;'>"+addTagSpan+"</p><span onclick='onClickRemoveTags("+quotes+tagSpanId+quotes+")' style='position:absolute;right:3px;top:3px;cursor:pointer'><img width='20px' src='https://www.freeiconspng.com/uploads/silver-close-button-png-15.png'></span>")
				    .appendTo($("#parentFormTag"));
				}
               }
		  }
		  /*if ( event.which == 16 ) {
		  	alert(tags.length +" "+tags);
		  }*/
		});
	});

	function TagFromRecent(tag){
		tagReplaceDash = tag.replace(/-/gi, " ");
		tags.push(tag);
		$("#inputTagsHiden").val(tags.join(","));
		var quotes = '"';
		$( "#tagsinput" ).val("");
		return $( "<div id='Tags"+tag+"'></div>" ).attr("style","background-color:#fff;color:black;font-size:14px; position:relative;display:inline-block;margin-right:5px;margin-top:3px")
		.append( "<p style='margin:0px 25px 0px 0px;padding:5px;'>"+tagReplaceDash+"</p><span onclick='onClickRemoveTags("+quotes+tag+quotes+")' style='position:absolute;right:3px;top:3px;cursor:pointer'><img width='20px' src='https://www.freeiconspng.com/uploads/silver-close-button-png-15.png'></span>")
		.appendTo($("#parentFormTag"));
	}

	function TagDefault(juduls){
		tagReplaceDash = juduls.replace(/ /gi, "-");
		for (var i = 0; i < judul.length; i++) {
			judul[i]=judul[i].replace('judulfilm', tagReplaceDash);
			var tagNoDash=judul[i].replace(/-/gi, ' ');
			tags.push(judul[i]);
			$("#inputTagsHiden").val(tags.join(","));
			var quotes = '"';
			$( "#tagsinput" ).val("");
			$( "<div id='Tags"+judul[i]+"'></div>" ).attr("style","background-color:#fff;color:black;font-size:14px; position:relative;display:inline-block;margin-right:5px;margin-top:3px")
			.append( "<p style='margin:0px 25px 0px 0px;padding:5px;'>"+tagNoDash+"</p><span onclick='onClickRemoveTags("+quotes+judul[i]+quotes+")' style='position:absolute;right:3px;top:3px;cursor:pointer'><img width='20px' src='https://www.freeiconspng.com/uploads/silver-close-button-png-15.png'></span>")
			.appendTo($("#parentFormTag"));
		}
	}

	function onClickRemoveTags(arrayName)
	{
		var i;
		for (i = 0 ; i < tags.length; i++) {
			if(tags[i] == arrayName)
			{
				$("#Tags"+arrayName).remove();
				tags.splice(i,1);
			}
		}
		$("#inputTagsHiden").val(tags.join(","));
	}

	$(document).ready(function(){
		$( "#submitform" ).keydown(function(e) {
		  var key = e.charCode || e.keyCode || 0;     
		  if (key == 13) {
		    e.preventDefault();
		  }
		});
		$('#tagsinput').keypress(function (e) {
		    var regex = new RegExp("^[a-zA-Z0-9\\-\\s]+$");
		    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		    if (regex.test(str)) {
		        return true;
		    }

		    e.preventDefault();
		    return false;
		});
	});